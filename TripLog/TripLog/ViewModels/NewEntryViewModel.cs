﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripLog.Models;
using TripLog.Services;
using Xamarin.Forms;

namespace TripLog.ViewModels
{
    public class NewEntryViewModel : BaseViewModel
    {
        public NewEntryViewModel(INavService navService) : base(navService)
        {
            
        }


        private string _title;
        private double _latitude;
        private double _longitude;
        private DateTime _date;
        private int _rating;
        private string _notes;

        public string Title
        {
            get { return _title; }
            set
            {
                if (value == _title) return;
                _title = value;
                OnPropertyChanged();
                SaveCommand.ChangeCanExecute();
            }
        }

        public double Latitude
        {
            get { return _latitude; }
            set
            {
                if (value.Equals(_latitude)) return;
                _latitude = value;
                OnPropertyChanged();
            }
        }

        public double Longitude
        {
            get { return _longitude; }
            set
            {
                if (value.Equals(_longitude)) return;
                _longitude = value;
                OnPropertyChanged();
            }
        }

        public DateTime Date
        {
            get { return _date; }
            set
            {
                if (value.Equals(_date)) return;
                _date = value;
                OnPropertyChanged();
            }
        }

        public int Rating
        {
            get { return _rating; }
            set
            {
                if (value == _rating) return;
                _rating = value;
                OnPropertyChanged();
            }
        }

        public string Notes
        {
            get { return _notes; }
            set
            {
                if (value == _notes) return;
                _notes = value;
                OnPropertyChanged();
            }
        }

        public NewEntryViewModel()
        {
            Date = DateTime.Today;
            Rating = 1;
        }

        Command _saveCommand;

        public Command SaveCommand
        {
            get { return _saveCommand ?? (_saveCommand = new Command(async () => await ExecuteSaveCommand(), CanSave)); }
        }

        async Task ExecuteSaveCommand()
        {
            var newItem = new TripLogEntry
            {
                Title = this.Title,
                Latitude = this.Latitude,
                Longitude = this.Longitude,
                Date = this.Date,
                Rating = this.Rating,
                Notes = this.Notes
            };
            // TODO: Implement logic to persist entry
            await NavService.GoBack();
        }

        bool CanSave()
        {
            return !String.IsNullOrWhiteSpace(Title);
        }

        public override async Task Init()
        {
            
        }
    }
}
