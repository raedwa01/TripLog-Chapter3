﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripLog.Models;
using TripLog.Services;

namespace TripLog.ViewModels
{
    public class DetailViewModel : BaseViewModel<TripLogEntry>
    {

        public DetailViewModel(INavService navService) : base(navService)
        {
            
        }
        TripLogEntry _entry;

        public TripLogEntry Entry
        {
            get { return _entry; }
            set
            {
                _entry = value; 
                OnPropertyChanged();
            }
        }

        public override async Task Init(TripLogEntry logEntry)
        {
            Entry = logEntry;
        }

    }
}
