﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TripLog.Services;
using TripLog.ViewModels;
using TripLog.Views;
using Xamarin.Forms;

namespace TripLog
{
    public class App : Application
    {
        public App()
        {
            // The root page of your application
            var mainpage = new NavigationPage(new MainPage());
            var navService = DependencyService.Get<INavService>() as XamarinFormsNavService;
            if (navService != null)
            {
                navService.XamarinFormsNav = mainpage.Navigation;

                navService.RegisterviewMapping(typeof(MainViewModel), typeof(MainPage));
                navService.RegisterviewMapping(typeof(DetailViewModel), typeof(DetailPage));
                navService.RegisterviewMapping(typeof(NewEntryViewModel), typeof(NewEntryPage));
            }

            MainPage = mainpage;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
