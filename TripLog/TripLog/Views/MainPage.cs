﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using TripLog.Models;
using TripLog.Services;
using TripLog.ViewModels;
using Xamarin.Forms;

namespace TripLog.Views
{
    public class MainPage : ContentPage
    {
        MainViewModel _vm => BindingContext as MainViewModel;

        public MainPage()
        {
            BindingContext= new MainViewModel(DependencyService.Get<INavService>());
            var newButton = new ToolbarItem
            {
                Text = "New"
            };

            //newButton.Clicked += (sender, args) =>
            //{
            //    Navigation.PushAsync(new NewEntryPage());
            //};
            newButton.SetBinding(ToolbarItem.CommandProperty, "NewCommand");
            ToolbarItems.Add(newButton);

            Title = "TripLog";
 
            var itemTemplate = new DataTemplate(typeof(TextCell));
            itemTemplate.SetBinding(TextCell.TextProperty, "Title");
            itemTemplate.SetBinding(TextCell.DetailProperty, "Notes");
            var entries = new ListView
            {
               // ItemsSource = items,
                ItemTemplate = itemTemplate
            };
            entries.SetBinding(ListView.ItemsSourceProperty, "LogEntries");
            entries.ItemTapped += async (sender, e) =>
            {
                var item = (TripLogEntry) e.Item;
                _vm.ViewCommand.Execute(item);
            };

            Content = entries;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            // Initialize MainViewModel
            if (_vm != null)
                await _vm.Init();
        }
    }
}
